# Biomaterials
Medical devices and implants must be made of biomaterials:  
https://en.wikipedia.org/wiki/Biomaterial

Biomaterial are nontoxic and don't react with body.  
They are biocompatible:  
https://en.wikipedia.org/wiki/Biocompatibility

## Some biomaterials
* https://en.wikipedia.org/wiki/Titanium_biocompatibility
* https://en.wikipedia.org/wiki/Cobalt-chrome
* https://en.wikipedia.org/wiki/Parylene
* https://en.wikipedia.org/wiki/Polyethylene

## Adhesives
* https://en.wikipedia.org/wiki/Cyanoacrylate

## Coating
* https://en.wikipedia.org/wiki/Titanium_nitride
* https://en.wikipedia.org/wiki/Bovine_submaxillary_mucin_coatings

An article covering biomaterials in cardiovascular surgery:  
https://doi.org/10.1016/B978-0-12-803581-8.02139-1

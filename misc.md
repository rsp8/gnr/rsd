# Miscellaneous

A bird's eye view of artificial organs:
https://en.wikipedia.org/wiki/Artificial_organ

A list to make sure we are not missing anything!:
https://en.wikipedia.org/wiki/List_of_organs_of_the_human_body

## Resolution: Machine Per Organ (MPO)
Since we are trying to build a wide range of machines
to replace various organs, at least at first iterations
we should focus on functions rather than organs.
So in case of digestive system, initially we work on
parenteral nutrition rather than stomach, intestine, ...
hence the resolution would be < 1 mpo.
On the other hand in interface, once we solved the
brain-computer interface problem, we will be able to add
more sensors than what human body already has, so the
resolution would be > 1 mpo, for the heart we have 1 mpo, ...

## Benchmark:
We need a gauge to measure success in our mission.
We know we are successful when we have build enough
machines to support a brain:
https://en.wikipedia.org/wiki/Isolated_brain
https://en.wikipedia.org/wiki/Brain_in_a_vat
https://en.wikipedia.org/wiki/Head_transplant
That might even be a potential treatment for some
complicated diseases that effect multiple organs
other than brain, like most cancers.
